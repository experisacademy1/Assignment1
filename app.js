let loanElement = document.getElementById("currentLoan")                         // Initialize all of the content needed from DOM
let payElement = document.getElementById("currentPay")
let payButton = document.getElementById("payButton")
let workButton = document.getElementById("workButton")
let loanButton = document.getElementById("loanButton")
let payLoanButton = document.getElementById("payLoanButton")
let cashElement = document.getElementById("currentMoney")
let komputersElement = document.getElementById("komputers")
let descriptionElement = document.getElementById("komputerText")
let specsElement = document.getElementById("specsElement")
let priceElement = document.getElementById("price")
let buyButton = document.getElementById("buyButton")
let computerImage = document.getElementById("computerImage")

let cash = 0;                                                                    // Initialize all variables that need to be "global"
let currentLoan = 0;
let payCheck = 0;
let loanAmount = 0;
let toPayOff = 0
let komputers = [];
let cart = [];


workButton.addEventListener("click", workButtonClicked)                            // Initialize all buttons
payButton.addEventListener("click", payButtonClicked)
loanButton.addEventListener("click", loanButtonClicked)
payLoanButton.addEventListener("click", payLoanButtonClicked)
payLoanButton.style.display = 'none'                                             // Hide Pay loan button until a loan is actually taken

fetch('https://noroff-komputer-store-api.herokuapp.com/computers')                              // API To fetch the JSON data
.then(response => response.json())
.then(data => komputers = data)
.then(komputers => addKomputerItems(komputers));

const addKomputerItems = (komputers) => {
    komputers.forEach(x => addKomputer(x));
}

const addKomputer = (komputer) => {                                              // I'ma be honest, this is the result of me writing code on friday, forgetting to comment what it does and here I am on monday wondering "wtf is this"
    const komputerElement = document.createElement("option")
    komputerElement.value = komputer.id 
    komputerElement.appendChild(document.createTextNode(komputer.title))
    komputersElement.appendChild(komputerElement);
    descriptionElement.innerHTML = "A little old, but it turns on."                         // Override initial selection so that "classic notebook" description still shows. It's a hack solution but it works.
    priceElement.innerHTML = "Costs: 200"
    specsElement.innerHTML = "Has a screen,Keyboard works, mostly,32MB Ram (Not upgradable),6GB Hard Disk,Comes with Floppy Disk Reader (Free) - Requires cable,Good excersice to carry"                                  
 }

const handleKomputerChange = e => {                                                                 // Whenever a new computer is selected from the dropdown menu, update the descriptions
    const selectedKomputer = komputers[e.target.selectedIndex];
    priceElement.innerHTML = "Price of laptop is: " + selectedKomputer.price;
    specsElement.innerText = selectedKomputer.specs;                                               
    descriptionElement.innerHTML = selectedKomputer.description;
    if (selectedKomputer.price === 33000) {                                                             // Sneaky teachers throw a PNG our way in the API so here's the fix for that
        computerImage.src ="https://noroff-komputer-store-api.herokuapp.com/assets/images/5.png"
    } else {
    computerImage.src = "https://noroff-komputer-store-api.herokuapp.com/" + selectedKomputer.image 
}
}

const handleBuyKomputer = () => {
    const selectedKomputer = komputers[komputersElement.selectedIndex]
    let price = selectedKomputer.price;
    
    if (cash >= price) {
        cash = cash - price
        document.getElementById("currentMoney").innerHTML="Your current cash is: " + cash;
        alert("You bought " + selectedKomputer.title + " !");
    } else {
        alert("Can't afford that! :(")
    }
    
}


buyButton.addEventListener("click", handleBuyKomputer)
komputersElement.addEventListener("change", handleKomputerChange)

function workButtonClicked () {                                                                                 // Click to work. If there's a loan, increment toPayOff to keep track of how much to pay to bank.
    if (currentLoan > 0) {
        toPayOff++;
    } 
        payCheck = payCheck +100;
    document.getElementById("currentPay").innerHTML="Your current paycheck is: " + payCheck; 
}

function payButtonClicked () {                                                                                   // Click to get paid. Will perform check if you have a loan and use 10% to pay off debt if true.
    if (payCheck == 0) {
        alert("No outstanding payments, work some more!")
    } else if (currentLoan > 0) { 
        let debt = toPayOff * 10;                                                                                // toPayOff is multiplied by 10 here, based on how many times you worked earlier while having a loan.
        currentLoan = currentLoan - debt
        payCheck = payCheck - debt
        cash = cash + payCheck;
        if(currentLoan < 0) {                                                                                    // Resolve bug that pops up if currentloan goes in the negative for whatever reason
            currentLoan = currentLoan * -1
            cash = cash + currentLoan;
            currentLoan = 0; 
            payLoanButton.style.display = 'none'
        }
        toPayOff = 0
        payCheck = 0
        document.getElementById("currentPay").innerHTML="Your current paycheck is: " + payCheck; 
        document.getElementById("currentMoney").innerHTML="Your current cash is: " + cash;
        document.getElementById("currentLoan").innerHTML="Your current loan is: " + currentLoan;
    
    }       
    
     else {
        cash = cash + payCheck;
        payCheck = 0;
        document.getElementById("currentPay").innerHTML="Your current paycheck is: " + payCheck; 
        document.getElementById("currentMoney").innerHTML="Your current cash is: " + cash;
    }
}

function loanButtonClicked() {                                                                                      // Click to take a loan. Checks if you have earlier loan, converts it to INT because int + string = no
    if (currentLoan == 0) {
    let loanInput = prompt("How much do you want to loan?", 0)
    loanAmount = parseInt(loanInput)
    if (Number.isNaN(loanAmount)) {                                                                                 // This makes sure the loan function doesn't break if rogue elements are added to input
        loanAmount = 0;
        document.getElementById("currentLoan").innerHTML="Your current loan is: " + currentLoan;
        payLoanButton.style.display = 'none'
    } 
    
    } else {
        alert("You already have a loan, can't loan twice")
    }

    let maxLoan = cash * 2 
    if (maxLoan < loanAmount) {
        alert("You can only loan 2x your current cash amount")
    } else if(loanAmount > 0) {
        cash = cash + loanAmount
        currentLoan = loanAmount
        document.getElementById("currentLoan").innerHTML="Your current loan is: " + currentLoan;
        document.getElementById("currentMoney").innerHTML="Your current cash is: " + cash;
        payLoanButton.style.display = 'inline'
      
    }

} 

function payLoanButtonClicked() {                                                   // To pay the loan off
   
         if (currentLoan === 0) {
        alert("You got no outstanding loans!")
        return                                                        // Using "return" exits function early and let's me reduce if/elses in the function
      } 
      let payAmount = payCheck

      if (payCheck === 0) {
        alert("No cash to pay off loan with :(")
        return
      }

        if (payCheck > currentLoan) {                                                               // If you have more paycheck than outstanding loan, pay the rest and close the loan
            alert("You paid the remaining " + currentLoan + " currency and closed your loan");
            payCheck = payCheck - currentLoan
            currentLoan = 0;
        }
        else{                                                                                       // Otherwise, pay what you have
        currentLoan = currentLoan - payAmount
        payCheck = payCheck - currentLoan;
    }

        if (currentLoan === 0) {
        payLoanButton.style.display = 'none' }
        else{
            alert("You paid off " + payAmount + " and your current debt is now " + currentLoan)
        }

        document.getElementById("currentLoan").innerHTML="Your current loan is: " + currentLoan;
        document.getElementById("currentMoney").innerHTML="Your current cash is: " + cash;
        document.getElementById("currentPay").innerHTML="Your current paycheck is: " + payCheck
        
    
}